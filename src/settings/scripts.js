var site;

function Site () {

	var instance, jQueries, internal;
	instance = this;
	internal = {};
	jQueries = {};

	function onReady ( _event ) {
	  window.isReady = true;
	  createCopyrightDate();
	}
  
  function createCopyrightDate() {
    var date = new Date();
    var year = date.getFullYear();
    document.getElementById("copyright-text").innerHTML = 'Copyright ' + year +  '. All Rights Reserved.';
  }

    
	jQuery( onReady );
	

}

site = new Site();